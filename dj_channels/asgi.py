"""
ASGI config for dj_channels project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/asgi/
"""

import os

from django.core.asgi import get_asgi_application
from channels.routing import ProtocolTypeRouter, URLRouter
from django.urls import path
from channels.auth import AuthMiddlewareStack

from app.routing import ws_patterns

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'dj_channels.settings')

application = get_asgi_application()
application = ProtocolTypeRouter({
    'websocket': AuthMiddlewareStack(URLRouter(
        ws_patterns
    ))
})
