import json

from channels.db import database_sync_to_async
from channels.exceptions import DenyConnection
from channels.generic.websocket import WebsocketConsumer
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import AnonymousUser
from asgiref.sync import async_to_sync
from channels.auth import login, get_user
from .models import User


class FetchDataConsumer(WebsocketConsumer):
    def connect(self):
        self.room_name = 'test'
        self.room_group_name = 'test_group'
        user = self.scope['user']
        try:
            user_obj = User.objects.get(id=1)  # For a moment using static user id
            user_obj.steps = 0
            user_obj.save()
            self.accept()
            self.send(
                text_data=json.dumps({'message': 'Connected', 'status': 200, 'success': True})
            )
        except:
            self.close()

    def receive(self, text_data):
        user = self.scope['user']
        try:
            user_obj = User.objects.get(id=1)
            steps = user_obj.steps
            status = json.loads(text_data).get('status')
            if status == 'next':
                f = open('1mb-test_json.json', )
                data = json.load(f)
                user_obj.steps = steps + 1
                user_obj.save()
                start = steps * 10
                end = start + 9
                self.send(
                    text_data=json.dumps({'data': data[start:end]})
                )
            else:
                user_obj.steps = 0
                user_obj.save()
                self.send(
                    text_data=text_data
                )
                self.close()
        except ObjectDoesNotExist:
            self.close()

    def disconnect(self, message):
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )
