from django.urls import path
from .consumer import FetchDataConsumer


ws_patterns = [
    path("ws/get_file/", FetchDataConsumer, name="json_data"),
]