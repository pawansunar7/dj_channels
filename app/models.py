from django.db import models


from django.contrib.sessions.models import Session
from django.conf import settings
from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, AbstractUser


# Create your models here.


class MyUserManager(BaseUserManager):

    def create_superuser(self, email, password):
        user = self.model(
            email=email
        )
        user.set_password(password)
        user.active = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

    def create_user(self, password=None, username=''):
        """
        Creates and saves a User with the given email and password.
        """
        if not self.email:
            raise ValueError('Users must have an Email Address')

        user = self.model(
            email=self.email,
            active=False,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    first_name = models.CharField(max_length=100, null=False, blank=False)
    last_name = models.CharField(max_length=100, null=False, blank=False)
    email = models.EmailField(max_length=100, null=False, blank=False)
    steps = models.IntegerField(default=0)

    objects = MyUserManager()
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ["email"]

    def get_short_name(self):
        return self.first_name

    def has_perm(self, perm, obj=None):
        return self.is_staff

    def has_module_perms(self, app_label):
        return self.is_superuser

    def __str__(self):
        return self.email
